#!/usr/bin/env python

from distutils.core import setup

setup (
    name         = 'django-quickpages',
    packages     = ['quickpages', 'quickpages.utils'],
    version      = '2.1',
    description  = 'Simliar to django-flatpages, but better & more flexible - also does snippets - v2+ Django2 and Python3 compatible',
    author       = 'Joseph Wolff',
    author_email = 'joe@eracks.com',
    url          = 'https://gitlab.com/jowolf/Django-QuickPages',
    download_url = 'https://gitlab.com/jowolf/Django-QuickPages/-/archive/master/Django-QuickPages-master.zip',
    license      = "BSD",
    install_requires = ['django-wysiwyg'],
)
